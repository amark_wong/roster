import request from '@/utils/request';

export async function queryRoles() {
	return request('role');
}

export async function queryEmployees() {
	return request('/employee');
}

export async function queryConfig() {
	return request('/config');
}

export async function queryShifts() {
	return request('/shifts');
}

export async function queryCredits() {
	return request('/credits');
}

export async function updateCredits(id,params) {
	return request({
		headers: {
            'Content-Type': 'application/json'
        },
		method: 'PUT',
		url: `credits/${id}`,
		data: params,
	});
}

export async function updateShift(id,params) {
	return request({
		headers: {
            'Content-Type': 'application/json'
        },
		method: 'PUT',
		url: `shifts/${id}`,
		data: params,
	});
}

export async function addShift(params) {
	return request({
		headers: {
            'Content-Type': 'application/json'
        },
		method: 'POST',
		url: `shifts`,
		data: params,
	});
}