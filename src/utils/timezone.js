import moment from 'moment-timezone';

export default function toggleTZ(time,timezone,utcflag)
{
    return utcflag ?
    moment.utc(time).format()
    :
    moment.tz(time,timezone).format();
}