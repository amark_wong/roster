import _ from 'lodash';
import moment from 'moment';
import { updateCredits, addShift } from '../services/api';
import toggleTZ from '../utils/timezone';
const ww = require('chinese-workday');
const isWorkday = ww.isWorkday;
const isHoliday = ww.isHoliday;
const isAddtionalWorkday = ww.isAddtionalWorkday;


const poolCheck = (curPool, pool, tier, staffs) => {
    // console.log('POOL check','curPool',curPool,'pool',pool,'tier',tier)
    if (tier === 1 && curPool.length === 0) {
        return [pool[1],2];
    }
    if (tier === 2 && curPool.length === 0) {
        pool = poolInit(staffs, pool = []);
        tier =1;
        return [pool[0],1];
    }else{
        return [curPool,tier];
    }
}


const removePool = (curPool, employee_id) => {
    return _.remove(curPool, item => { return item.employee_id === employee_id })
}

const fetchNightShift = (shifts, date) => {
    return _.filter(shifts.find(item => item.start_time.slice(0, 11) === date && (item.role_id === 1 || item.role_id === 6)))
}

const fetchLastEmployee = (shifts, date) => {
    const lastWeekShift = (moment(date).isoWeekday() === 3) ? moment(date).day(-2) :
        (moment(date).isoWeekday() === 2) ? moment(date).day(-1) : false;
    if (!lastWeekShift) {
        return shifts.find(shift => (shift.start_date.slice(0, 11) === lastWeekShift) && (shift.role_id === 1 || shift.role_id === 6)).employee_id
    } else {
        return null;
    }
}

const fetchYDAEmployee = (shifts, date) => {
    console.log('shifts',shifts.length)
    //find yesterday in shifts dataset 
    return shifts.find(shift => shift.start_time.slice(0, 10) === moment(date).subtract(1, 'days').format("YYYY-MM-DD")
        //find night shift or holiday shift
        && (shift.role_id === 1 || shift.role_id === 6)).employee_id
}

const fetchDBYEmployee = (shifts, date) => {
    //find yesterday in shifts dataset 
    console.log('DBY shifts', shifts.map(x => x.start_time.slice(0, 10)))
    return shifts.find(shift => shift.start_time.slice(0, 10) === moment(date).subtract(2, 'days').format("YYYY-MM-DD")
        //find night shift or holiday shift
        && (shift.role_id === 1 || shift.role_id === 6)).employee_id
}

const poolInit = (staffs, pool = []) => {
    console.log('init',staffs,'pool',pool);
    console.log('to be pushed tier 1', _.filter(staffs, ['tier', 1]).map(x => { return { "employee_id": x.id } }));
    console.log('to be pushed tier 2', _.filter(staffs, ['tier', 2]).map(x => { return { "employee_id": x.id } }));
    console.log('to be pushed tier 3', _.filter(staffs, ['tier', 3]).map(x => { return { "employee_id": x.id } }));
    for (var j = 1; j <= 3; j++) {
        pool.push(_.filter(staffs, ['tier', j]).map(x => { return { "employee_id": x.id } }));
    }
    console.log('INIT pool',pool);
    return pool;
}

const updateCredit = (credits, employee_id, role_id) => {
    console.log('credits',credits,'employee_id',employee_id,'role_id',role_id)
    return credits[employee_id - 1].credit[role_id - 1]++;
}


export default function generate(shifts, staffs, start_date, end_date, roles, credits) {
    // const end_date = moment(start_date).add(length, 'days');
    var curDate = start_date;
    var curEmployee = null;
    var pool = [];
    let curShifts = shifts;
    const YDA = moment(start_date).subtract(1, 'days');
    const DBY = moment(start_date).subtract(2, 'days').format('YYYY-MM-DD');
    const duration = moment.duration(moment(end_date).diff(moment(start_date))).asDays();
    pool = poolInit(staffs);
    var curPool = poolInit(staffs)[0];
    var newShift = null;
    var tier = 1;

    for (let i = 0; i < duration; i++) {
        //choose different shift for workday an holiday
        // console.log('curDate',moment(curDate).format())
        if (!isHoliday(curDate)) {
            //yesterday night shift or holiday shift should be ruled out
            // console.log('curPool',curPool,'pool',pool,'tier',tier);
            removePool(curPool, fetchYDAEmployee(shifts, curDate));
            curPool = poolCheck(curPool,pool,tier,staffs)[0];
            tier= poolCheck(curPool,pool,tier,staffs)[1];
            // console.log('curPool',curPool,'pool',pool,'tier',tier);
            removePool(curPool, fetchDBYEmployee(shifts, curDate));
            curPool = poolCheck(curPool,pool,tier,staffs)[0];
            tier= poolCheck(curPool,pool,tier,staffs)[1];
            // console.log('curPool',curPool,'pool',pool);
            //generate 5 shifts
            for (let role_id = 1; role_id <= 5; role_id++) {
                if (curPool.length === 1) {
                    console.log('curlPool', curPool[0].employee_id)
                    curEmployee = curPool[0].employee_id;
                }
                else {
                    let finalPool = _.intersection(curPool.map(x => x.employee_id), credits.map(x => x.id))
                    console.log('finalPool',finalPool)
                    if (finalPool.length===0){
                        curPool = poolCheck(finalPool,pool,tier,staffs)[0];
                        tier = poolCheck(finalPool,pool,tier,staffs)[1];
                        console.log('reset curPool',curPool,'new tier',tier)
                        finalPool = _.intersection(curPool.map(x => x.employee_id), credits.map(x => x.id));
                        console.log('reset finalPool',finalPool,'new tier',tier)
                    }
                    curEmployee = finalPool.length === 1 ? finalPool[0].employee_id : _.sample(finalPool);
                    // console.log('finalPool',finalPool)

                }
                newShift = {
                    "employee_id": curEmployee,
                    "start_time": moment(curDate).format('YYYY-MM-DD') + "T" + roles.find(role => role.id === role_id).start_time + "+08:00",
                    "role_id": role_id,
                    "end_time": moment(moment(curDate).format('YYYY-MM-DD') + "T" + roles.find(role => role.id === role_id).start_time + "+08:00")
                        .add(roles.find(role => role.id === role_id).duration, 'hours').format()
                }
                addShift(newShift);
                shifts.push(newShift);
                // console.log('credits', credits, 'employee', curEmployee, 'role_id', role_id);
                updateCredit(credits, curEmployee, role_id);
                removePool(curPool, curEmployee);
                curPool = poolCheck(curPool,pool,tier,staffs)[0];
                tier = poolCheck(curPool,pool,tier,staffs)[1];
                console.log('new tier', tier)

                //     if (isWorkday(curDate) || isAddtionalWorkday(curDate)) {
                //         //remove yesterday night shift staff id from pool
                //         let lastShift = _.find(shifts, (item) => { return (item.role_id === 1 || item.role_id === 6) }).employee_id;
                //         let curPool = _.remove(pool[tier - 1], item => { return (item.employee_id !== lastShift) });
                //         //special rule, 
                //         if (moment(curDate).day() === 3) {
                //             let lastFriShift = _.find(shifts, (item) => { return (item.role_id === 1 || item.role_id === 6) }).employee_id;
                //             let curPool = _.remove(pool[tier - 1], item => { return (item.employee_id !== lastShift) });
                //         }
                //         if (moment(curDate).day() === 2) {
                //             let lastSatShift = _.find(shifts, (item) => { return (item.role_id === 1 || item.role_id === 6) }).employee_id;
                //             let curPool = _.remove(pool[tier - 1], item => { return (item.employee_id !== lastShift) })
                //         }
                //         //generate day shift A&B, noon shift, night shift as well as standby
                //         const roles = [1, 2, 3, 4, 5];
                //         const curShifts = roles.map(role => {
                //             //check the pool, switch to tier 2 if tier 1 is empty, replendish the pool 1 and 2 if tier is empty
                //             if (!curPool.length && tier === 1) {
                //                 curPool = pool[1];
                //                 tier = 2;
                //             }
                //             //unfinished
                //             //remove people who have already been assigned 
                //             curPool = _.remove(pool[tier], (item) => { return (item.employee_id === curShift) })
                //             //get the employees with lowest credit at the specfic role
                //             let credit = credits.map(item => { return { "id": item.id, "credit": item.credit[role - 1] } });
                //             let creditMin = _.min(credit);
                //             credit = _.filter(credit, x => x.credit===creditMin);
                //             //randomly select a employee_id
                //             employee_id = _.sample(credit);
                //             //remove this employee_id from pool and add his credit
                //             curPool = _.remove(curPool)
                //             //move to next role
                //             return {
                //                 "employee_id": employee_id,
                //                 "start_time": moment(date) + role.start_time,
                //                 "role_id": role,
                //                 "end_time": moment(date) + role.end_time,
                //             }
                //         }

                // }
            }
        }
        else {
            //holiday still need to remove the night of yesterday
            removePool(curPool, fetchYDAEmployee(shifts, curDate));
            for (let role_id = 6; role_id <= 7; role_id++) {
                if (curPool.length === 1) {
                    console.log('curlPool', curPool[0].employee_id)
                    curEmployee = curPool[0].employee_id;
                }
                else {
                    let finalPool = _.intersection(curPool.map(x => x.employee_id), credits.map(x => x.id))
                    console.log('finalPool',finalPool)
                    if (finalPool.length===0){
                        curPool = poolCheck(finalPool,pool,tier,staffs)[0];
                        tier = poolCheck(finalPool,pool,tier,staffs)[1];
                        console.log('reset curPool',curPool)
                        finalPool = _.intersection(curPool.map(x => x.employee_id), credits.map(x => x.id));
                        console.log('reset finalPool',finalPool)
                    }
                    curEmployee = finalPool.length === 1 ? finalPool[0].employee_id : _.sample(finalPool);
                    // console.log('finalPool',finalPool)

                }
                newShift = {
                    "employee_id": curEmployee,
                    "start_time": moment(curDate).format('YYYY-MM-DD') + "T" + roles.find(role => role.id === role_id).start_time + "+08:00",
                    "role_id": role_id,
                    "end_time": moment(moment(curDate).format('YYYY-MM-DD') + "T" + roles.find(role => role.id === role_id).start_time + "+08:00")
                        .add(roles.find(role => role.id === role_id).duration, 'hours').format()
                }
                addShift(newShift);
                shifts.push(newShift);
                // console.log('credits', credits, 'employee', curEmployee, 'role_id', role_id);
                updateCredit(credits, curEmployee, role_id);
                removePool(curPool, curEmployee);
                curPool = poolCheck(curPool,pool,tier,staffs)[0];
                tier = poolCheck(curPool,pool,tier,staffs)[1];
                console.log('new tier', tier)
            }
        }
        curDate=moment(curDate).add(1,'days');
    }
}