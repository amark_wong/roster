import axios from 'axios';

const baseURL = 'http://localhost:4000'
const withCredentials = true;
const timeout = 30000;

const axiosInstance = axios.create({
	baseURL,
	withCredentials,
	timeout,
});

/**
 * Requests a path, returning a promise.
 *
 * @param  {string} path       The path we want to request
 * @param  {object} [option] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export default function request(options) {
	return axiosInstance(options)
		.then(response => response)
		.catch(error => error);
}
