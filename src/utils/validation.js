import _ from 'lodash';
import moment from 'moment';
export default function validation(employee_id, new_start_time, new_end_time, shifts, id) {
    //get the other shifts of the employee
    const shiftslist = shifts.filter(x => x.employee_id === employee_id && x.id !== id).map(y => (
        {
            id: y.id,
            start_time: y.start_time,
            end_time: y.end_time
        })
    );
    //compare the new start time with closet end time
    const timeDiffEnd = shiftslist.map(x => (
        moment.duration(
            moment(x.start_time)
                .diff(
                    (_.isNumber(new_end_time) ?
                        moment(new_end_time, 'x')
                        :
                        moment(new_end_time))
                )
        ).asHours()
    )).filter(value => value > 0);
    const timeDiffStart = shiftslist.map(x => (
        moment.duration(
            (_.isNumber(new_start_time) ?
                moment(new_start_time, 'x')
                :
                moment(new_start_time))
                    .diff(
                        moment(x.end_time)
                    )
        ).asHours()
    )).filter(value => value > 0);
    //ensure 12 consecutive hours rest
    if (timeDiffEnd.length && timeDiffStart.length) {
        if (_.min(timeDiffEnd) >= 12 && _.min(timeDiffStart) >= 12) {
            return true;
        } else {
            return false;
        }
    } else {
        if (timeDiffEnd.length) {
            if (_.min(timeDiffEnd) >= 12) {
                return true;
            } else {
                return false;
            }
        } else {
            if (timeDiffStart.length) {
                if (_.min(timeDiffStart) >= 12) {
                    return true;
                } else {
                    return false;
                }
            }
            else{
                return true;
            }
        }
    }
}