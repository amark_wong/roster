import _ from 'lodash';
import moment from 'moment';
import React from 'react';
import { connect } from 'dva';
import { Table, message } from 'antd';
import EditableCell, {EditableFormRow} from './EditableCell';
import validation from '../utils/validation';


@connect(({ roster }) => ({
    roster
  }))
class RosterTable extends React.Component {
    constructor(props) {
        super(props);
        this.columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                width: '30%',
                filters: _.uniq(props.data.map(x => x.name)).map(x => ({
                    text: x,
                    value: x,
                })),
                onFilter: (value, record) => record.name.indexOf(value) === 0,
                sorter: (a, b) => a.name.length - b.name.length,
                sortDirections: ['descend', 'ascend'],
            },
            {
                title: 'Role',
                dataIndex: 'role',
                filters: _.uniq(props.data.map(x => x.role)).map(x => ({
                    text: x,
                    value: x,
                })),
                onFilter: (value, record) => record.name.indexOf(value) === 0,
                sorter: (a, b) => a.role.length - b.role.length,
                sortDirections: ['descend', 'ascend'],
            },
            {
                title: 'Start time',
                dataIndex: 'start_time',
                editable: true,
            },
            {
                title: 'End time',
                dataIndex: 'end_time',
                editable: true,
            },
            {
                title: 'Break duration',
                dataIndex: 'break_duration',
            },
        ];
        this.state = {
            dataSource: this.props.data
        };
    }


    handleSave = row => {
        const { dispatch, roster } = this.props;
        const newData = [...this.state.dataSource];
        const index = newData.findIndex(item => row.key === item.key);
        const item = newData[index];
        newData.splice(index, 1, {
            ...item,
            ...row,
        });
        const employee_id = _.find(roster.employees, 
            {first_name: _.split(row.name,' ',2)[0],
            last_name: _.split(row.name,' ',2)[1]}).id;
        if (validation(employee_id,row.start_time,row.end_time,roster.shifts,row.id))
        {
            let data= {
                'employee_id': employee_id,
                'start_time': moment.utc(row.start_time).format(),
                'role_id':  _.find(roster.roles, {name:row.role}).id,
                'end_time': moment.utc(row.end_time).format(),
                'break_duration': 3600,
            }
            this.setState({ dataSource: newData });
            dispatch({
                type: 'roster/updateShift',
                payload: { id: row.id, data: data } 
            })
        }else{
            message.warning('No enough consecutive rest time!');
        }
    };

    componentDidMount()
    {
        const { dispatch } = this.props;
        dispatch({
          type: 'roster/fetchShifts',
        });
    }
    render() {
        const components = {
            body: {
                row: EditableFormRow,
                cell: EditableCell,
            },
        };
        const columns = this.columns.map(col => {
            if (!col.editable) {
                return {
                    ...col,
                };
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    editable: col.editable,
                    dataIndex: col.dataIndex,
                    title: col.title,
                    handleSave: this.handleSave,
                }),
            };
        });
        return (
            <div>
                <Table
                    components={components}
                    rowClassName={() => 'editable-row'}
                    bordered
                    dataSource={this.state.dataSource}
                    columns={columns}
                    rowKey={this.state.dataSource.id}
                />
            </div>
        );
    }
}
export default RosterTable;