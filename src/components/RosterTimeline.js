import React, { Component } from "react";
import Timeline from "react-calendar-timeline";
import { connect } from 'dva';
import 'react-calendar-timeline/lib/Timeline.css';
import validation from '../utils/validation';
import moment from 'moment';
import _ from 'lodash';

var keys = {
  groupIdKey: "id",
  groupTitleKey: "title",
  groupRightTitleKey: "rightTitle",
  itemIdKey: "id",
  itemTitleKey: "title",
  itemDivTitleKey: "title",
  itemGroupKey: "group",
  itemTimeStartKey: "start",
  itemTimeEndKey: "end",
  groupLabelKey: "title"
};

@connect(({ roster }) => ({
  roster
}))

class RosterTimeline extends Component {
  constructor(props) {
    super(props);

    const { groups, items, defaultTimeStart, defaultTimeEnd } = props;

    this.state = {
      groups,
      items,
      defaultTimeStart,
      defaultTimeEnd
    };
  }
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'roster/fetchShifts',
    });
  }

  itemRenderer = ({ item, timelineContext, itemContext, getItemProps, getResizeProps }) => {
    const { left: leftResizeProps, right: rightResizeProps } = getResizeProps();
    const backgroundColor = itemContext.selected ? (itemContext.dragging ? "red" : item.selectedBgColor) : item.bgColor;
    const borderColor = itemContext.resizing ? "red" : item.color;
    return (
      <div
        {...getItemProps({
          style: {
            backgroundColor,
            color: item.color,
            borderColor,
            borderStyle: "solid",
            borderWidth: 1,
            borderRadius: 4,
            borderLeftWidth: itemContext.selected ? 3 : 1,
            borderRightWidth: itemContext.selected ? 3 : 1
          },
          onMouseDown: () => {
          }
        })}
      >
        {itemContext.useResizeHandle ? <div {...leftResizeProps} /> : null}

        <div
          style={{
            height: itemContext.dimensions.height,
            overflow: "hidden",
            paddingLeft: 3,
            textOverflow: "ellipsis",
            whiteSpace: "nowrap"
          }}
        >
          {itemContext.title}
        </div>

        {itemContext.useResizeHandle ? <div {...rightResizeProps} /> : null}
      </div>
    );
  };

  handleItemMove = (itemId, dragTime, newGroupOrder) => {
    const { items, groups } = this.state;

    const { dispatch, roster } = this.props;

    const group = groups[newGroupOrder];

    this.setState({
      items: items.map(item =>
        item.id === itemId
          ? Object.assign({}, item, {
            start: dragTime,
            end: dragTime + (item.end - item.start),
            group: group.id
          })
          : item
      )
    });
    const item = items.filter(x => x.id === itemId)[0];
    if (validation(item.group, dragTime, dragTime + (item.end - item.start), roster.shifts, itemId)) {
      let data = {
        'employee_id': item.group,
        'start_time': moment.utc(dragTime).format(),
        'role_id': _.find(roster.roles, { name: item.title }).id,
        'end_time': moment.utc(dragTime + (item.end - item.start)).format(),
        'break_duration': 3600,
      }
      dispatch({
        type: 'roster/updateShift',
        payload: { id: itemId, data: data }
      });
    }
    else {
      this.setState({
        items: items.map(item =>
          item.id === itemId
            ? Object.assign({}, item, {
              start: item.start,
              end: item.end,
              group: group.id
            })
            : item
        )
      });
    }
  };

  handleItemResize = (itemId, time, edge) => {
    const { items } = this.state;
    const { dispatch, roster } = this.props;

    this.setState({
      items: items.map(item =>
        item.id === itemId
          ? Object.assign({}, item, {
            start: edge === "left" ? time : item.start,
            end: edge === "left" ? item.end : time
          })
          : item
      )
    });
    const item = items.filter(x => x.id === itemId)[0];
    if (validation(item.group, edge === "left" ? time : item.start, edge === "left" ? item.end : time, roster.shifts, itemId, 'timeline')) {
      let data = {
        'employee_id': item.group,
        'start_time': moment.utc(edge === "left" ? time : item.start).format(),
        'role_id': _.find(roster.roles, { name: item.title }).id,
        'end_time': moment.utc(edge === "left" ? item.end : time).format(),
        'break_duration': 3600,
      }
      dispatch({
        type: 'roster/updateShift',
        payload: { id: itemId, data: data }
      });
    }
    else {
      this.setState({
        items: items.map(item =>
          item.id === itemId
            ? Object.assign({}, item, {
              start: item.start,
              end: item.end,
            })
            : item
        )
      });
    }
  };

  render() {
    const { groups, items, defaultTimeStart, defaultTimeEnd } = this.state;

    return (
      <Timeline
        groups={groups}
        items={items}
        keys={keys}
        fullUpdate
        itemTouchSendsClick={false}
        stackItems
        itemHeightRatio={0.85}
        canMove={true}
        canResize={"both"}
        defaultTimeStart={defaultTimeStart}
        defaultTimeEnd={defaultTimeEnd}
        itemRenderer={this.itemRenderer}
        onItemMove={this.handleItemMove}
        onItemResize={this.handleItemResize}
      />
    );
  }
}
export default RosterTimeline;