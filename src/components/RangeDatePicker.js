import React from 'react';
import _ from 'lodash';
import { DatePicker, Button } from 'antd';
import { connect } from 'dva';
import generate from '../utils/generate';
import moment from 'moment';

const { RangePicker } = DatePicker;

const poolInit = (staffs, pool = []) => {
    console.log('init',staffs,'pool',pool);
    console.log('to be pushed tier 1', _.filter(staffs, ['tier', 1]).map(x => { return { "employee_id": x.id } }));
    console.log('to be pushed tier 2', _.filter(staffs, ['tier', 2]).map(x => { return { "employee_id": x.id } }));
    console.log('to be pushed tier 3', _.filter(staffs, ['tier', 3]).map(x => { return { "employee_id": x.id } }));
    for (var j = 1; j <= 3; j++) {
        pool.push(_.filter(staffs, ['tier', j]).map(x => { return { "employee_id": x.id } }));
    }
    console.log('INIT pool',pool);
    return pool;
}

@connect(({ roster }) => ({
    roster
}))
class RangeDatePicker extends React.Component {
    constructor(props) {
        super(props);
        const { data } = props;
        this.state = {
            data,
            start_date:moment(),
            end_date:moment(new Date()).add(1,'days'),
          };
        // console.log('DatePicker', data);
    }

    onChange= (date, dateString)=> {
        // console.log('date',date, 'dateString',dateString);
        this.setState({ start_date: dateString[0] });
        this.setState({ end_date: dateString[1] });
        // console.log('state',this.state.start_date)
        // console.log('state',this.state.end_date)
    }

    generate(shifts, staffs, start_date, end_date, roles, credits){
        var curDate = start_date;
        var curEmployee = null;
        var pool = [];
        let curShifts = shifts;
        const YDA = moment(start_date).subtract(1, 'days');
        const DBY = moment(start_date).subtract(2, 'days').format('YYYY-MM-DD');
        const duration = moment.duration(moment(end_date).diff(moment(start_date))).asDays();
        pool = poolInit(staffs);
        var curPool = poolInit(staffs)[0];
        var newShift = null;
        var tier = 1;

    }

    fetchNightShift = (shifts, date) => {
        return _.filter(shifts.find(item => item.start_time.slice(0, 11) === date && (item.role_id === 1 || item.role_id === 6)))
    }

    fetchLastEmployee = (shifts, date) => {
        const lastWeekShift = (moment(date).isoWeekday() === 3) ? moment(date).day(-2) :
            (moment(date).isoWeekday() === 2) ? moment(date).day(-1) : false;
        if (!lastWeekShift) {
            return shifts.find(shift => (shift.start_date.slice(0, 11) === lastWeekShift) && (shift.role_id === 1 || shift.role_id === 6)).employee_id
        } else {
            return null;
        }
    }

    fetchYDAEmployee = (shifts, date) => {
        console.log('shifts',shifts.length)
        //find yesterday in shifts dataset 
        return shifts.find(shift => shift.start_time.slice(0, 10) === moment(date).subtract(1, 'days').format("YYYY-MM-DD")
            //find night shift or holiday shift
            && (shift.role_id === 1 || shift.role_id === 6)).employee_id
    }
    
    fetchDBYEmployee = (shifts, date) => {
        //find yesterday in shifts dataset 
        console.log('DBY shifts', shifts.map(x => x.start_time.slice(0, 10)))
        return shifts.find(shift => shift.start_time.slice(0, 10) === moment(date).subtract(2, 'days').format("YYYY-MM-DD")
            //find night shift or holiday shift
            && (shift.role_id === 1 || shift.role_id === 6)).employee_id
    }

    updateCredit = (credits, employee_id, role_id) => {
        return credits[employee_id - 1].credit[role_id - 1]++;
    }

    poolCheck = (curPool, pool, tier, staffs) => {
        // console.log('POOL check','curPool',curPool,'pool',pool,'tier',tier)
        if (tier === 1 && curPool.length === 0) {
            return [pool[1],2];
        }
        if (tier === 2 && curPool.length === 0) {
            pool = poolInit(staffs, pool = []);
            tier =1;
            return [pool[0],1];
        }else{
            return [curPool,tier];
        }
    }

    removePool = (curPool, employee_id) => {
        return _.remove(curPool, item => { return item.employee_id === employee_id })
    }

    onGenerate = ()=> {
        const { data } = this.props;
        // const { dispatch } = this.props;
        const { start_date, end_date } = this.state;
        // console.log('state date',start_date, end_date)
        // console.log('dateString',dateString);
        // console.log('ONGENERATE',data,start_date,end_date);
        console.log('data',data,'start_date',start_date,'end_date',end_date);
        generate(data.shifts, data.employees, start_date, end_date, data.roles, data.credits)

        // const shifts=generate()
        // dispatch({
        //     type: 'roster/addShifts',
        //     // payload:{data:data}
        // })
        // generate(dateString)
    }

    componentDidMount() {
        // const { dispatch } = this.props;
        // dispatch({
        //     type: 'roster/fetchShifts',
        // });
    }

    render() {
        // console.log('roster',roster);
        const { data, start_date, end_date } = this.state;
        return (
            <div>
                <RangePicker onChange={this.onChange} />
                <Button type="primary" onClick={this.onGenerate}>Generate roster</Button>
            </div>
        )
    }
}

export default RangeDatePicker;