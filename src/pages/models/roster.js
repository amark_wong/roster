import _ from 'lodash';
import { queryConfig, queryEmployees, queryRoles, queryShifts, queryCredits, updateCredits, updateShift, addShift } from '@/services/api';

export default {
	namespace: 'roster',

	state: {
		config: {},
		employees: [],
		roles: [],
		shifts: [],
	},

	effects: {
		*fetchShifts(_, { call, put }) {
			const configResponse = yield call(queryConfig);
			yield put({
				type: 'listConfig',
				payload: configResponse,
			});
			const rolesResponse = yield call(queryRoles);
			yield put({
				type: 'listRoles',
				payload: rolesResponse,
			});
			const employeesResponse = yield call(queryEmployees);
			yield put({
				type: 'listEmployees',
				payload: employeesResponse,
			});
			const shiftsResponse = yield call(queryShifts);
			yield put({
				type: 'listShifts',
				payload: shiftsResponse,
			});
			const creditsResponse = yield call(queryCredits);
			yield put({
				type: 'listCredits',
				payload: creditsResponse,
			});
		},
		*updateCredits({ payload }, { call, put }) {
			yield call(updateCredits, payload.id, payload.data);
			yield put({
				type: 'saveCredit',
				payload,
			});
		},
		*updateShift({ payload }, { call, put }) {
			yield call(updateShift, payload.id, payload.data);
			yield put({
				type: 'saveShift',
				payload,
			});
		},
		*addShift({ payload }, { call, put }) {
			yield call(addShift, payload.data);
			yield put({
				type: 'saveShift',
				payload,
			});
		},
	},
	reducers: {
		listConfig(state, action) {
			const result = {
				...state,
				config: action.payload.data
			};
			return result;
		},
		listEmployees(state, action) {
			const result = {
				...state,
				employees: action.payload.data
			};
			return result;
		},
		listRoles(state, action) {
			const result = {
				...state,
				roles: action.payload.data
			};
			return result;
		},
		listShifts(state, action) {
			const result = {
				...state,
				shifts: action.payload.data
			};
			return result;
		},
		listCredits(state, action) {
			const result = {
				...state,
				credits: action.payload.data
			};
			return result;
		},
		saveShift(state, action) {
			let newShifts = _.dropwhile(state.shifts,['id',action.payload.id])
			newShifts.push({
				id:action.payload.id,
			...action.payload.data})
			const result = {
				...state,
				shifts:newShifts,
			};
			return result;
		},
		saveCredit(state, action) {
			let newCredits = _.dropwhile(state.credits,['id',action.payload.id])
			newCredits.push({
				id:action.payload.id,
			...action.payload.data})
			const result = {
				...state,
				shifts:newCredits,
			};
			return result;
		}
	},
};