import _ from 'lodash';
import moment from 'moment';
import React, { Component } from 'react';
import shallowCompare from 'shallow-compare';
import { connect } from 'dva';
import { Tabs, Spin } from 'antd';
import RosterTable from '../../components/RosterTable';
import RosterTimeline from '../../components/RosterTimeline';
import RangeDatePicker from '../../components/RangeDatePicker';
import toggleTZ from '../../utils/timezone';
// import generate from '../../utils/generate';

const TabPane = Tabs.TabPane;

@connect(({ roster }) => ({
  roster
}))

class Roster extends Component {
  constructor(props) {
    super(props);
    const { roster } = this.props;

    this.state = {
      config: roster.config,
      employees: roster.employees,
      roles: roster.roles,
      shifts: roster.shifts,
      credits: roster.credits,
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'roster/fetchShifts',
    });
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return shallowCompare(this, nextProps, nextState);
  // }


  render() {
    const { roster } = this.props;
    const allLoaded = !!(roster.employees.length
      && roster.shifts.length
      && roster.roles.length
      && !_.isEmpty(roster.config));
    const timezone = allLoaded ? roster.config.timezone : 'Australia/Perth';
    let groups = allLoaded ?
      roster.employees.map(e => ({
        id: e.id,
        title: e.first_name + ' ' + e.last_name,
      }))
      :
      [];
    let roster_calendar = allLoaded ? roster.shifts.map(s => ({
      id: s.id,
      group: s.employee_id,
      title: roster.roles.find(x => x.id === s.role_id).name,
      start: moment(s.start_time),
      end: moment(s.end_time),
      canMove: true,
      canResize: true,
      canChangeGroup: false,
      bgColor: roster.roles.find(x => x.id === s.role_id).background_colour,
      color: roster.roles.find(x => x.id === s.role_id).text_colour,
    }))
      :
      [];
    let roster_table = allLoaded ? roster.shifts.map(item => ({
      key: item.id,
      id: item.id,
      name: item.employee_id && roster.employees.length ?
        roster.employees.find(x => x.id === item.employee_id).first_name + ' '
        +
        roster.employees.find(x => x.id === item.employee_id).last_name + ' '
        :
        'unknown'
      ,
      role: item.role_id && roster.roles.length ?
        roster.roles.find(x => x.id === item.role_id).name
        :
        'unknown'
      ,
      start_time: moment(toggleTZ(item.start_time, timezone, false)).format('YYYY-MM-DD HH:mm'),
      end_time: moment(toggleTZ(item.end_time, timezone, false)).format('YYYY-MM-DD HH:mm'),
      break_duration: item.break_duration,
    })) :
      [];

    const startTimeline = toggleTZ(_.min(roster.shifts.map(x => x.start_time)), timezone, false);
    const endTimeline = toggleTZ(_.max(roster.shifts.map(x => x.end_time)), timezone, false);
    return (
      <div>
        <Tabs defaultActiveKey="2" >
          <Tabs.TabPane tab="Tabluar view" key="1">
            {roster_table.length ?
              <RosterTable data={roster_table}></RosterTable>
              :
              <Spin tip="Loading tabular view..." size="large"></Spin>
            }
          </Tabs.TabPane>
          <TabPane tab="Timeline view" key="2">
            {(groups.length && roster_calendar.length)
              ?
              <RosterTimeline groups={groups} items={roster_calendar}
                defaultTimeStart={moment(startTimeline).add(-12, 'hour')}
                defaultTimeEnd={moment(endTimeline).add(12, 'hour')}></RosterTimeline>
              :
              <Spin tip="Loading timeline view..." size="large"></Spin>
            }
          </TabPane>
        </Tabs>
        <RangeDatePicker data={roster} />
      </div >
    )
  }
}
export default Roster;

