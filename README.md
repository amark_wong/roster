**Assumptions**

*I assumed that the "back-to-back shifts" means workers cannot get a consecutive 12 hours rest, according to the website of Ministry of Labor(https://www.labour.gov.on.ca/english/es/pubs/hours/rest_2.php). Therefore, in this project, a shift can be prolonged or moved, as long as the duration between two shifts is longer than 12 hours.*

*Also I assumed the 3600(seconds?) break is included in the shift, and how to arrange this time is totally up to workers. Thus, I would consider a one hour early tap off plus only 11 consecutive rest as a violation of the Ministry of Labor Regulation.*

*The limit of overtime is not considered in this project, which should in a real project.*

---

## Run
I've just switched to umi+dva boilerplate this Tuesday and no very familar with these instruments, so the installation might take a little bit longer.

1. npm install
2. npm audit fix; npm install json-server -g
3. cd ..; npm install create-umi -g; create-umi roster
4. choose app with arrow key and enter
5. type N for typescript option
6. select antd and dva, using space key
7. for all the conflicts, choose no
8. rm -rf pages/index.js
9. open another tab or terminal, in the roster directory run mock_backend.sh
10. npm start
---

## Todo list

1. Some styling, we are using the default style of the libraries.
2. Validate whether the overtime is legal and acceptable.
3. Optimizations, such prevent re-render.

---
